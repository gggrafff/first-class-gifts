FROM python:3.8.10

ARG PROJNAME=gifts_first_class

ENV PROJNAME=${PROJNAME}
ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /${PROJNAME}
WORKDIR /${PROJNAME}

# Install linux packages
RUN apt-get update && \
apt-get install --no-install-recommends -y nginx supervisor gettext && \
rm -rf /var/lib/apt/lists/* && \
apt-get purge --auto-remove && \
apt-get clean

# Install gunicorn
RUN pip3 install --no-cache-dir gunicorn

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN echo "directory=/${PROJNAME}" >> /etc/supervisor/conf.d/supervisord.conf

# Setup nginx
COPY config/flask.conf /etc/nginx/sites-available/flask.conf.template
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Install python dependencies
COPY requirements.txt .
RUN pip install -r ./requirements.txt

# Setup flask application
COPY application .

# Start processes with setting listen port
CMD /bin/bash -c "envsubst < /etc/nginx/sites-available/flask.conf.template > /etc/nginx/sites-available/default" && /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf