import os


class Configuration(object):
    DEBUG = False
    MODEL_PATH = os.path.dirname(os.path.abspath(__file__)) + "/resources/cb_model_top5_60point970.pkl"
    COLUMNS = [
        'st_code_snd',
        'st_code_rsv',
        'date_depart_month',
        'date_depart_week',
        'date_depart_day',
        'date_depart_hour',
        'fr_id',
        'route_type',
        'is_load',
        'rod',
        'common_ch',
        'vidsobst',
        'distance',
        'snd_org_id',
        'rsv_org_id',
        'snd_roadid',
        'rsv_roadid',
        'snd_dp_id',
        'rsv_dp_id'
    ]
