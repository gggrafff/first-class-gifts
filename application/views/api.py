from flask_restx import Resource
from flask_restx import fields

from app import app, api, dispatcher
from models.api_models import calculation_request_model


ns = api.namespace('inference', description='Use the model')


@ns.route('/calc')
class CalcDeliveryTime(Resource):
    """Calculates how long will the delivery take"""
    @ns.doc('Make a predict')
    @ns.expect(calculation_request_model)
    @ns.response(fields.Float, required=True, description='Predicted delivery duration')
    def post(self):  # TODO: GET will be more REST-like way here
        """Calculates how long will the delivery take"""
        model_input = {
            'date_depart_week': api.payload['date_depart_week'],
            'distance': api.payload['distance'],
            'rsv_org_id': api.payload['rsv_org_id'],
            'snd_dp_id': api.payload['snd_dp_id'],
            'rsv_dp_id': api.payload['rsv_dp_id'],
        }
        duration = dispatcher.calc_delivery_time(**model_input)
        return duration
