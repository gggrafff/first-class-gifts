from flask import request, render_template, send_from_directory
from app import app, dispatcher
import os


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == 'POST':
        model_input = {
            'date_depart_week': int(request.form['date_depart_week']),
            'distance': float(request.form['distance']),
            'rsv_org_id': int(request.form['rsv_org_id']),
            'snd_dp_id': int(request.form['snd_dp_id']),
            'rsv_dp_id': int(request.form['rsv_dp_id']),
        }
        duration = dispatcher.calc_delivery_time(**model_input)
        return render_template("index.html", duration=("%.2f" % duration))
    else:
        return render_template("index.html")
