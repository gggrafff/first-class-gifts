from flask import Flask
from flask_restx import Api
from config import Configuration
from models.dispatcher import Dispatcher

app = Flask(__name__)
app.config.from_object(Configuration)
app.url_map.strict_slashes = False

api = Api(app, version='0.1', title='Gifts first class',
          description='MADE. HackWagon. First class gifts.',
          contact='greal-san@ya.ru',
          license='GPL',
          prefix='/api/',
          doc='/api/'
          )

dispatcher = Dispatcher(app.config['MODEL_PATH'])
