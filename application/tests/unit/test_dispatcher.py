import pytest
from app import app, dispatcher


# python -m pytest tests/

@pytest.mark.parametrize("model_input", [
    ('railwaypgkhappyrailway:)railway',
        'digitalhappynewyeardigital:)wagon', 2021, 4, 15, 17, 16, 2261.0,
        3.0, 0, 8, 9.0, 111.0, 16.0, 2974, 6682, 21, 21, 111, 111),
    ('railwaytutupgkpgk:)digital', 'pgkdigitalrailway:):)happy', 2021,
        5, 17, 2, 7, 1089.0, 3.0, 1, 8, 9.0, 111.0, 879.0, 2082, 10729,
        9, 9, 67, 23)
])
def test_calc_delivery_time(model_input):
    prediction = dispatcher.calc_delivery_time(**dict(zip(app.config['COLUMNS'], model_input)))
    assert prediction > 0
    assert prediction < 150
