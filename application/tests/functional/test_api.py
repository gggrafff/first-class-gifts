import pytest

from app import app
from views.api import *  # noqa # it's important


# python -m pytest tests/


@pytest.fixture(name='client')
def initialize_authorized_test_client():
    app.testing = True
    client = app.test_client()
    yield client
    app.testing = False


@pytest.mark.post
def test_explain_word(client):
    data = {'st_code_snd': 'railwaypgkhappyrailway:)railway',
            'st_code_rsv': 'digitalhappynewyeardigital:)wagon',
            'date_depart_year': 2021,
            'date_depart_month': 4,
            'date_depart_week': 15,
            'date_depart_day': 17,
            'date_depart_hour': 16,
            'fr_id': 2261.0,
            'route_type': 3.0,
            'is_load': 0,
            'rod': 8,
            'common_ch': 9.0,
            'vidsobst': 111.0,
            'distance': 16.0,
            'snd_org_id': 2974,
            'rsv_org_id': 6682,
            'snd_roadid': 21,
            'rsv_roadid': 21,
            'snd_dp_id': 111,
            'rsv_dp_id': 111}
    response = client.post('/api/inference/calc', json=data)
    assert response.status_code == 200
    response_json_data = response.get_json()
    assert response_json_data > 0
    assert response_json_data < 150
