from flask_restx import fields
from app import api

calculation_request_model = api.model('CalculationRequest', {
    'st_code_snd': fields.String(required=False, description='Source stationd id'),
    'st_code_rsv': fields.String(required=False, description='Destination station id'),

    'date_depart_year': fields.Integer(required=False, min=2019, max=2025, description='Departure year'),
    'date_depart_month': fields.Integer(required=False, min=1, max=12, description='Departure month'),
    'date_depart_week': fields.Integer(required=True, min=1, max=53, description='Departure week'),
    'date_depart_day': fields.Integer(required=False, min=1, max=31, description='Departure day'),
    'date_depart_hour': fields.Integer(required=False, min=0, max=23, description='Departure hour'),

    'fr_id': fields.Integer(required=False, min=0, description='Cargo id'),
    'route_type': fields.Integer(required=False, min=1, max=10, description='Route type'),
    'is_load': fields.Integer(required=False, min=0, max=1, description='Is the wagon loaded (0 - no, 1 - yes)?'),
    'rod': fields.Integer(required=False, min=1, max=20, description='Type of rolling stock'),
    'common_ch': fields.Integer(required=False, min=0, description='Id of the common characteristics of the wagon'),
    'vidsobst': fields.Integer(required=False, min=1, max=2000, description='Type of ownership'),
    'distance': fields.Float(required=True, min=0, description='Distance'),

    'snd_org_id': fields.Integer(required=False, min=0, description='Sender id'),
    'rsv_org_id': fields.Integer(required=True, min=0, description='Receiver id'),
    'snd_roadid': fields.Integer(required=False, min=0, description='Source road id'),
    'rsv_roadid': fields.Integer(required=False, min=0, description='Destionation road id'),
    'snd_dp_id': fields.Integer(required=True, min=0, description='Source region id'),
    'rsv_dp_id': fields.Integer(required=True, min=0, description='Destionation region id'),
})
