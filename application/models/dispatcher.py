import os.path
import pickle


class AbstractDispatcher:
    def __init__(self):
        pass

    def calc_delivery_time(self, **kwargs):
        raise NotImplementedError()


class StubDispatcher(AbstractDispatcher):
    def __init__(self):
        super().__init__()

    def calc_delivery_time(self, **kwargs):
        return 42


class Dispatcher(AbstractDispatcher):
    def __init__(self, model_path):
        super().__init__()
        if os.path.isfile(model_path):
            self.model = pickle.load(open(model_path, 'rb'))
            self.features = ['distance', 'rsv_dp_id', 'date_depart_week', 'snd_dp_id', 'rsv_org_id']
        else:
            raise Exception("Invalid model path")

    def calc_delivery_time(self, **kwargs):
        input = []
        for feature_name in self.features:
            input.append(kwargs[feature_name])
        result = self.model.predict(input)
        if result < 0:
            result = 1
        return result
